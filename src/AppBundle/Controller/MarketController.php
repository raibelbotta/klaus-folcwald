<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Market;
use AppBundle\Form\Type\MarketFilterFormType;
use AppBundle\Services\BittrexUpdater;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * MarketController
 *
 * @Route("/markets")
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class MarketController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET"})
     * @return Response
     */
    public function indexAction()
    {
        $manager = $this->getDoctrine()->getManager();
        $lastUpdateTime = $manager->getRepository('AppBundle:SystemConfig')
            ->getValue(BittrexUpdater::SC_MARKET_UPDATION_LAST_TIME);

        $filter = $this->createForm(MarketFilterFormType::class);

        return $this->render('@App/Market/index.html.twig', array(
            'last_update_time' => $lastUpdateTime ? new \DateTime($lastUpdateTime) : null,
            'filter' => $filter->createView()
        ));
    }

    /**
     * @Route("/results", options={"expose": true})
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexResults(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $qb = $manager->getRepository('AppBundle:Market')
            ->createQueryBuilder('m');

        $columns = $request->get('columns');
        $orders = $request->get('order', array());

        $filter = $this->createForm(MarketFilterFormType::class);
        $filter->submit($request->query->get($filter->getName()));
        $this->container->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filter, $qb);

        if ($orders) {
            $column = call_user_func(function($name) {
                if ($name == 'name') {
                    return 'm.name';
                } elseif ($name == 'shortName') {
                    return 'm.marketCurrency';
                } elseif ($name == 'longName') {
                    return 'm.marketCurrencyLong';
                }
                return null;
            }, $columns[$orders[0]['column']]['name']);
            if (null !== $column) {
                $qb->orderBy($column, strtoupper($orders[0]['dir']));
            }
        }

        $paginator = $this->get('knp_paginator');
        $page = $request->get('start', 0) / $request->get('length') + 1;
        $pagination = $paginator->paginate($qb->getQuery(), $page, $request->get('length'));
        $total = $pagination->getTotalItemCount();

        $template = $this->container->get('twig')->load('@App/Market/_index_cells.html.twig');
        $data = array_map(function(Market $record) use($template) {
            return array(
                $record->getName(),
                $record->getMarketCurrency(),
                $record->getMarketCurrencyLong(),
                $template->renderBlock('created_at', array('record' => $record)),
                $template->renderBlock('is_active', array('record' => $record)),
                $template->renderBlock('is_tracked', array('record' => $record))
            );
        }, $pagination->getItems());

        return new JsonResponse(array(
            'data' => $data,
            'draw' => $request->get('draw'),
            'recordsTotal' => $total,
            'recordsFiltered' => $total
        ));
    }

    /**
     * @Route("/{id}/set-state/{state}", requirements={"id": "\d+", "state": "on|off"}, options={"expose": true})
     * @param Market $market
     * @param string $state
     * @return JsonResponse
     */
    public function setStateAction(Market $market, $state)
    {
        $market->setIsTracking($state === 'on');
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse();
    }
}
