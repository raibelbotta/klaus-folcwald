<?php

namespace AppBundle\Controller;

use Carbon\Carbon;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 *
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method({"GET"})
     * @return Response
     */
    public function indexAction()
    {
        return $this->render("@App/Default/index.html.twig");
    }

    /**
     * @Route("/stats", options={"expose": true})
     * @Method("GET")
     * @return JsonResponse
     */
    public function getStatisticsAction()
    {
        $manager = $this->getDoctrine()->getManager();

        $laps = array(30, 90, 300, 3600);
        $results = array();

        foreach ($laps as $lap) {
            $query = $manager->createQuery('SELECT m.name AS market, COUNT(mt.id) AS cantidad, SUM(mt.total)AS total, MAX(mt.price) AS max_price, MIN(mt.price) AS min_price FROM AppBundle:MarketTrade AS mt JOIN mt.market AS m WHERE m.isTracking = true AND mt.date >= :date GROUP BY m.id')
                ->setParameter('date', Carbon::now('0000')->sub(new \DateInterval(sprintf('PT%sS', $lap)))->format('Y-m-d H:i:s'));

            $results[$lap] = $query->getResult();
        }

        return new JsonResponse($results);
    }

    /**
     * @Route("/news")
     * @Method("GET")
     * @return Response
     */
    public function newsDashbaordAction()
    {
        $laps = array(10, 20, 30, 40, 50, 60, 24 * 60, 48 * 60, 72 * 60);
        $manager = $this->getDoctrine()->getManager();
        $repository = $manager->getRepository('AppBundle:CurrencyNews');
        $results = array();

        foreach ($laps as $lapKey => $lap) {
            if ($lapKey < 6) {
                $to = Carbon::now();
                $from = Carbon::instance($to)->subMinutes($lap);

                $query = $repository->createQueryBuilder('n')
                    ->select(array('c.shortName, c.longName, SUM(n.countByShortname), SUM(n.countByFullName)'))
                    ->join('n.currency', 'c')
                    ->where('n.createdAt > :from AND n.createdAt <= :to')
                    ->groupBy('c.shortName', 'c.longName')
                    ->setParameters(array(
                        'from' => $from->format('Y-m-d H:i:s'),
                        'to' => $to->format('Y-m-d H:i:s')
                    ))
                    ->getQuery();

                foreach ($query->getResult() as $data) {
                    $results[$data[1]]['name'] = $data[2];
                    $results[$data[1]][$lap] = array(
                        'short' => $data[3],
                        'long' => $data[4]
                    );
                }
            }
        }

        die(dump($results));

        return $this->render('@App/Default/news_dashboard.html.twig');
    }
}
