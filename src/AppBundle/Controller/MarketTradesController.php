<?php

namespace AppBundle\Controller;

use AppBundle\Entity\MarketTrade;
use AppBundle\Form\Type\MarketTradeFilterFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * MarketTradesController
 *
 * @Route("/market-trades")
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class MarketTradesController extends Controller
{
    /**
     * @Route("/")
     * @Method("GET")
     * @return Response
     */
    public function indexAction()
    {
        $form = $this->createForm(MarketTradeFilterFormType::class);

        return $this->render('@App/MarketTrade/index.html.twig', array(
            'filter' => $form->createView()
        ));
    }

    /**
     * @Route("/results", options={"expose": true})
     * @Method("GET")
     * @param Request $request
     * @return JsonResponse
     */
    public function indexResultsAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $qb = $manager->getRepository('AppBundle:MarketTrade')
            ->createQueryBuilder('mt')
            ->join('mt.market', 'm');

        $columns = $request->get('columns');
        $orders = $request->get('order', array());

        $filter = $this->createForm(MarketTradeFilterFormType::class);
        $filter->submit($request->query->get($filter->getName()));
        $this->container->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filter, $qb);

        if ($orders) {
            $column = call_user_func(function($name) {
                if ($name === 'market') {
                    return 'm.name';
                } elseif ($name === 'transactionId') {
                    return 'mt.transactionId';
                } elseif ($name === 'date') {
                    return 'mt.date';
                } elseif ($name === 'orderType') {
                    return 'mt.orderType';
                }

                return null;
            }, $columns[$orders[0]['column']]['name']);
            if (null !== $column) {
                $qb->orderBy($column, strtoupper($orders[0]['dir']));
            }
        }

        $paginator = $this->get('knp_paginator');
        $page = $request->get('start', 0) / $request->get('length') + 1;
        $pagination = $paginator->paginate($qb->getQuery(), $page, $request->get('length'));
        $total = $pagination->getTotalItemCount();

        $template = $this->container->get('twig')->load('@App/MarketTrade/_index_cells.html.twig');
        $data = array_map(function(MarketTrade $record) use($template) {
            return array(
                $record->getMarket()->getName(),
                $record->getTransactionId(),
                $template->renderBlock('date', array('record' => $record)),
                $template->renderBlock('quantity', array('record' => $record)),
                $template->renderBlock('price', array('record' => $record)),
                $template->renderBlock('total', array('record' => $record)),
                $record->getOrderType()
            );
        }, $pagination->getItems());

        return new JsonResponse(array(
            'data' => $data,
            'draw' => $request->get('draw'),
            'recordsTotal' => $total,
            'recordsFiltered' => $total
        ));
    }
}
