<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Currency;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * CurrenciesController
 *
 * @Route("/currencies")
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class CurrenciesController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET"})
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('@App/Currencies/index.html.twig');
    }

    /**
     * @Route("/results", options={"expose": true})
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function indexResultsAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $qb = $manager->getRepository('AppBundle:Currency')
            ->createQueryBuilder('c');

        $columns = $request->get('columns');
        $orders = $request->get('order', array());

//        $filter = $this->createForm(MarketFilterFormType::class);
//        $filter->submit($request->query->get($filter->getName()));
//        $this->container->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filter, $qb);

        if ($orders) {
            $column = call_user_func(function($name) {
//                if ($name == 'name') {
//                    return 'm.name';
//                } elseif ($name == 'shortName') {
//                    return 'm.marketCurrency';
//                } elseif ($name == 'longName') {
//                    return 'm.marketCurrencyLong';
//                }
                return null;
            }, $columns[$orders[0]['column']]['name']);
            if (null !== $column) {
                $qb->orderBy($column, strtoupper($orders[0]['dir']));
            }
        }

        $paginator = $this->get('knp_paginator');
        $page = $request->get('start', 0) / $request->get('length') + 1;
        $pagination = $paginator->paginate($qb->getQuery(), $page, $request->get('length'));
        $total = $pagination->getTotalItemCount();

        $template = $this->container->get('twig')->load('@App/Market/_index_cells.html.twig');
        $data = array_map(function(Currency $record) use($template) {
            return array(
                $record->getShortName(),
                $record->getLongName(),
                sprintf('%0.8f', $record->getTxFee()),
                $template->renderBlock('created_at', array('record' => $record)),
                $template->renderBlock('is_active', array('record' => $record))
            );
        }, $pagination->getItems());

        return new JsonResponse(array(
            'data' => $data,
            'draw' => $request->get('draw'),
            'recordsTotal' => $total,
            'recordsFiltered' => $total
        ));
    }
}
