<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * CurrencyNews
 *
 * @ORM\Table(
 *     name="currency_news",
 *     indexes={@ORM\Index(name="idx_currency_news_created_at", columns={"created_at"})}
 * )
 * @ORM\Entity
 */
class CurrencyNews
{
    /**
     * @var integer
     *
     * @ORM\Column(type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Currency
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Currency")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $currency;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_by_shortname", type="integer")
     */
    private $countByShortname;

    /**
     * @var integer
     *
     * @ORM\Column(name="count_by_full_name", type="integer")
     */
    private $countByFullName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countByShortname
     *
     * @param integer $countByShortname
     *
     * @return CurrencyNews
     */
    public function setCountByShortname($countByShortname)
    {
        $this->countByShortname = $countByShortname;

        return $this;
    }

    /**
     * Get countByShortname
     *
     * @return integer
     */
    public function getCountByShortname()
    {
        return $this->countByShortname;
    }

    /**
     * Set countByFullName
     *
     * @param integer $countByFullName
     *
     * @return CurrencyNews
     */
    public function setCountByFullName($countByFullName)
    {
        $this->countByFullName = $countByFullName;

        return $this;
    }

    /**
     * Get countByFullName
     *
     * @return integer
     */
    public function getCountByFullName()
    {
        return $this->countByFullName;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CurrencyNews
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set currency
     *
     * @param \AppBundle\Entity\Currency $currency
     *
     * @return CurrencyNews
     */
    public function setCurrency(\AppBundle\Entity\Currency $currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \AppBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
