<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MarketTrade
 *
 * @ORM\Table(name="market_trade", indexes={@ORM\Index(name="idx_market_trade_created_at", columns={"created_at"})})
 * @ORM\Entity
 */
class MarketTrade
{
    /**
     * @var integer
     *
     * @ORM\Column(type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Market
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Market")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $market;

    /**
     * @var integer
     *
     * @ORM\Column(name="transaction_id", type="bigint", unique=true)
     */
    private $transactionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=20, scale=8)
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=20, scale=8)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=20, scale=8)
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="fill_type", length=20)
     */
    private $fillType;

    /**
     * @var string
     *
     * @ORM\Column(name="order_type", length=4)
     * @Assert\Regex("/^(SELL|BUY)$/")
     */
    private $orderType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set transactionId
     *
     * @param integer $transactionId
     * @return MarketTrade
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Get transactionId
     *
     * @return integer
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return MarketTrade
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     * @return MarketTrade
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return MarketTrade
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return MarketTrade
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set fillType
     *
     * @param string $fillType
     * @return MarketTrade
     */
    public function setFillType($fillType)
    {
        $this->fillType = $fillType;

        return $this;
    }

    /**
     * Get fillType
     *
     * @return string
     */
    public function getFillType()
    {
        return $this->fillType;
    }

    /**
     * Set orderType
     *
     * @param string $orderType
     * @return MarketTrade
     */
    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;

        return $this;
    }

    /**
     * Get orderType
     *
     * @return string
     */
    public function getOrderType()
    {
        return $this->orderType;
    }

    /**
     * Set market
     *
     * @param \AppBundle\Entity\Market $market
     * @return MarketTrade
     */
    public function setMarket(\AppBundle\Entity\Market $market)
    {
        $this->market = $market;

        return $this;
    }

    /**
     * Get market
     *
     * @return \AppBundle\Entity\Market
     */
    public function getMarket()
    {
        return $this->market;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return MarketTrade
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
