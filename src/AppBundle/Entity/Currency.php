<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Currency
 *
 * @ORM\Table(name="currency")
 * @ORM\Entity
 * @UniqueEntity(fields={"shortName"})
 */
class Currency
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(length=50)
     */
    private $shortName;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $longName;

    /**
     * @var integer
     *
     * @ORM\Column(name="min_confirmation", type="integer")
     */
    private $minConfirmation;

    /**
     * @var float
     *
     * @ORM\Column(name="tx_fee", type="decimal", precision=10, scale=8)
     */
    private $txFee;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", options={"default": true})
     */
    private $isActive;

    /**
     * @var string
     *
     * @ORM\Column(length=50)
     */
    private $coinType;

    /**
     * @var string
     *
     * @ORM\Column(length=50, nullable=true)
     */
    private $baseAddress;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true)
     */
    private $notice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $newsUpdatedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     * @return Currency
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Set longName
     *
     * @param string $longName
     * @return Currency
     */
    public function setLongName($longName)
    {
        $this->longName = $longName;

        return $this;
    }

    /**
     * Get longName
     *
     * @return string
     */
    public function getLongName()
    {
        return $this->longName;
    }

    /**
     * Set minConfirmation
     *
     * @param integer $minConfirmation
     * @return Currency
     */
    public function setMinConfirmation($minConfirmation)
    {
        $this->minConfirmation = $minConfirmation;

        return $this;
    }

    /**
     * Get minConfirmation
     *
     * @return integer
     */
    public function getMinConfirmation()
    {
        return $this->minConfirmation;
    }

    /**
     * Set txFee
     *
     * @param string $txFee
     * @return Currency
     */
    public function setTxFee($txFee)
    {
        $this->txFee = $txFee;

        return $this;
    }

    /**
     * Get txFee
     *
     * @return string
     */
    public function getTxFee()
    {
        return $this->txFee;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Currency
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set coinType
     *
     * @param string $coinType
     * @return Currency
     */
    public function setCoinType($coinType)
    {
        $this->coinType = $coinType;

        return $this;
    }

    /**
     * Get coinType
     *
     * @return string
     */
    public function getCoinType()
    {
        return $this->coinType;
    }

    /**
     * Set baseAddress
     *
     * @param string $baseAddress
     * @return Currency
     */
    public function setBaseAddress($baseAddress)
    {
        $this->baseAddress = $baseAddress;

        return $this;
    }

    /**
     * Get baseAddress
     *
     * @return string
     */
    public function getBaseAddress()
    {
        return $this->baseAddress;
    }

    /**
     * Set notice
     *
     * @param string $notice
     * @return Currency
     */
    public function setNotice($notice)
    {
        $this->notice = $notice;

        return $this;
    }

    /**
     * Get notice
     *
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Currency
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set newsUpdatedAt
     *
     * @param \DateTime $newsUpdatedAt
     *
     * @return Currency
     */
    public function setNewsUpdatedAt($newsUpdatedAt)
    {
        $this->newsUpdatedAt = $newsUpdatedAt;

        return $this;
    }

    /**
     * Get newsUpdatedAt
     *
     * @return \DateTime
     */
    public function getNewsUpdatedAt()
    {
        return $this->newsUpdatedAt;
    }
}
