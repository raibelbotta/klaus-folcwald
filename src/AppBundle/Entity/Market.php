<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Market
 *
 * @ORM\Table(name="market")
 * @ORM\Entity
 */
class Market
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(length=10)
     */
    private $marketCurrency;

    /**
     * @var string
     *
     * @ORM\Column(length=10)
     */
    private $baseCurrency;

    /**
     * @var string
     *
     * @ORM\Column(length=100)
     */
    private $marketCurrencyLong;

    /**
     * @var string
     *
     * @ORM\Column(length=100)
     */
    private $baseCurrencyLong;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=12, scale=8)
     */
    private $minTradeSize;

    /**
     * @var string
     *
     * @ORM\Column(length=50, unique=true)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true)
     */
    private $notice;

    /**
     * @var string
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isSponsored;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true)
     */
    private $logoUrl;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isTracking;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="history_updated_at", type="datetime", nullable=true)
     */
    private $historyUpdatedAt;

    /**
     * Market constructor
     */
    public function __construct()
    {
        $this->isActive = true;
        $this->isTracking = true;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marketCurrency
     *
     * @param string $marketCurrency
     * @return Market
     */
    public function setMarketCurrency($marketCurrency)
    {
        $this->marketCurrency = $marketCurrency;

        return $this;
    }

    /**
     * Get marketCurrency
     *
     * @return string
     */
    public function getMarketCurrency()
    {
        return $this->marketCurrency;
    }

    /**
     * Set baseCurrency
     *
     * @param string $baseCurrency
     * @return Market
     */
    public function setBaseCurrency($baseCurrency)
    {
        $this->baseCurrency = $baseCurrency;

        return $this;
    }

    /**
     * Get baseCurrency
     *
     * @return string
     */
    public function getBaseCurrency()
    {
        return $this->baseCurrency;
    }

    /**
     * Set marketCurrencyLong
     *
     * @param string $marketCurrencyLong
     * @return Market
     */
    public function setMarketCurrencyLong($marketCurrencyLong)
    {
        $this->marketCurrencyLong = $marketCurrencyLong;

        return $this;
    }

    /**
     * Get marketCurrencyLong
     *
     * @return string
     */
    public function getMarketCurrencyLong()
    {
        return $this->marketCurrencyLong;
    }

    /**
     * Set baseCurrencyLong
     *
     * @param string $baseCurrencyLong
     * @return Market
     */
    public function setBaseCurrencyLong($baseCurrencyLong)
    {
        $this->baseCurrencyLong = $baseCurrencyLong;

        return $this;
    }

    /**
     * Get baseCurrencyLong
     *
     * @return string
     */
    public function getBaseCurrencyLong()
    {
        return $this->baseCurrencyLong;
    }

    /**
     * Set minTradeSize
     *
     * @param string $minTradeSize
     * @return Market
     */
    public function setMinTradeSize($minTradeSize)
    {
        $this->minTradeSize = $minTradeSize;

        return $this;
    }

    /**
     * Get minTradeSize
     *
     * @return string
     */
    public function getMinTradeSize()
    {
        return $this->minTradeSize;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Market
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Market
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Market
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Market
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Market
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set notice
     *
     * @param string $notice
     * @return Market
     */
    public function setNotice($notice)
    {
        $this->notice = $notice;

        return $this;
    }

    /**
     * Get notice
     *
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }

    /**
     * Set isSponsored
     *
     * @param boolean $isSponsored
     * @return Market
     */
    public function setIsSponsored($isSponsored)
    {
        $this->isSponsored = $isSponsored;

        return $this;
    }

    /**
     * Get isSponsored
     *
     * @return boolean
     */
    public function getIsSponsored()
    {
        return $this->isSponsored;
    }

    /**
     * Set logoUrl
     *
     * @param string $logoUrl
     * @return Market
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    /**
     * Get logoUrl
     *
     * @return string
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     * Set isTracking
     *
     * @param boolean $isTracking
     * @return Market
     */
    public function setIsTracking($isTracking)
    {
        $this->isTracking = $isTracking;

        return $this;
    }

    /**
     * Get isTracking
     *
     * @return boolean
     */
    public function getIsTracking()
    {
        return $this->isTracking;
    }

    /**
     * Set historyUpdatedAt
     *
     * @param \DateTime $historyUpdatedAt
     * @return Market
     */
    public function setHistoryUpdatedAt($historyUpdatedAt)
    {
        $this->historyUpdatedAt = $historyUpdatedAt;

        return $this;
    }

    /**
     * Get historyUpdatedAt
     *
     * @return \DateTime
     */
    public function getHistoryUpdatedAt()
    {
        return $this->historyUpdatedAt;
    }
}
