<?php

namespace AppBundle\GoogleNews\API;

/**
 * Connection
 *
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class Connection
{
    const END_POINT = 'https://newsapi.org';
    const VERSION = '/v2';

    private $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @param string $q
     */
    public function getEverything($q)
    {
        return $this->sendRequest('/everything', array('q' => trim($q)));
    }

    private function sendRequest($methodName, $parameters = array())
    {
        $stringfiedParameters = $this->stringfyParameters(array_merge($parameters, array('apiKey' => $this->apiKey)));

        $uri = sprintf('%s%s%s%s', self::END_POINT, self::VERSION, $methodName, $stringfiedParameters ? sprintf('?%s', $stringfiedParameters) : '');

        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);

        if (!$response) {
            throw new \Exception(sprintf('Null response in "%s". Error: %s', $uri, curl_error($ch)));
        }

        $obj = json_decode($response);

        if (null === $obj) {
            throw new \Exception(sprintf('Invalid API response [response content: %s] [API request URL: %s]', $response, $uri));
        }

        return $obj;
    }

    private function stringfyParameters($parameters)
    {
        $params = array();
        foreach ($parameters as $name => $value) {
            $params[] = sprintf('%s=%s', $name, urlencode($value));
        }

        return implode('&', $params);
    }
}
