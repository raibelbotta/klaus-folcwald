<?php

namespace AppBundle\Services;

use AppBundle\Bittrex\API\PublicMethods;
use AppBundle\Entity\Currency;
use AppBundle\Entity\Market;
use AppBundle\Entity\MarketTrade;
use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr;

/**
 * BittrexUpdater
 *
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class BittrexUpdater
{
    const SC_MARKET_UPDATION_LAP = 'BITTREX_MARKET_UPDATE_LAP';
    const SC_MARKET_UPDATION_LAST_TIME = 'BITTREX_MARKET_UPDATE_LAST_TIME';
    const SC_CURRENCIES_UPDATION_LAP = 'BITTREX_CURRENCIES_UPDATE_LAP';
    const SC_CURRENCIES_UPDATION_LAST_TIME = 'BITTREX_CURRENCIES_UPDATE_LAST_TIME';

    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var PublicMethods
     */
    private $publicApi;

    /**
     * BittrexUpdater constructor
     * @param EntityManager $manager
     * @param PublicMethods $publicApi
     */
    public function __construct(EntityManager $manager, PublicMethods $publicApi)
    {
        $this->manager = $manager;
        $this->publicApi = $publicApi;
    }

    /**
     * @param bool $force
     */
    public function updateMarkets($force = false)
    {
        $nextUpdationTime = $this->getNextUpdationTime(self::SC_MARKET_UPDATION_LAST_TIME, self::SC_MARKET_UPDATION_LAP, 6 * 60 * 60);
        $now = new \DateTime('now');

        if ($force || ($nextUpdationTime < $now)) {
            $apiMarkets = $this->publicApi->getMarkets();
            $markets = $this->manager->getRepository('AppBundle:Market')->findAll();

            foreach ($markets as $index => $market) {
                $markets[$market->getName()] = $market;
                unset($markets[$index]);
            }

            foreach ($apiMarkets as $index => $market) {
                if (!isset($markets[$market->MarketName])) {
                    $markets[$market->MarketName] = new Market();
                    $markets[$market->MarketName]->setName($market->MarketName);
                    $this->manager->persist($markets[$market->MarketName]);
                }

                $markets[$market->MarketName]
                    ->setMarketCurrency($market->MarketCurrency)
                    ->setBaseCurrency($market->BaseCurrency)
                    ->setMarketCurrencyLong($market->MarketCurrencyLong)
                    ->setBaseCurrencyLong($market->BaseCurrencyLong)
                    ->setMinTradeSize($market->MinTradeSize)
                    ->setIsActive($market->IsActive)
                    ->setCreationDate(new \DateTime($market->Created))
                    ->setNotice($market->Notice)
                    ->setIsSponsored($market->IsSponsored)
                    ->setLogoUrl($market->LogoUrl);
            }

            $this->manager->flush();

            $this->updateLastUpdationTime(self::SC_MARKET_UPDATION_LAST_TIME, new \DateTime('now'));
        }

        return $this;
    }

    public function updateCurrencies($force = false)
    {
        $nextUpdationTime = $this->getNextUpdationTime(self::SC_CURRENCIES_UPDATION_LAST_TIME, self::SC_CURRENCIES_UPDATION_LAP, 6 * 60 * 60);
        $now = new \DateTime('now');

        if ($force || ($nextUpdationTime < $now)) {
            $apiCurrencies = $this->publicApi->getCurrencies();
            $currencies = $this->manager->getRepository('AppBundle:Currency')->findAll();

            foreach ($currencies as $index => $currency) {
                $currencies[$currency->getShortName()] = $currency;
                unset($currencies[$index]);
            }

            foreach ($apiCurrencies as $index => $currency) {
                $name = $currency->Currency;

                if (!isset($currencies[$name])) {
                    $currencies[$name] = new Currency();
                    $currencies[$name]->setShortName($currency->Currency);
                    $this->manager->persist($currencies[$name]);
                }

                $currencies[$name]
                    ->setLongName($currency->CurrencyLong)
                    ->setMinConfirmation($currency->MinConfirmation)
                    ->setTxFee($currency->TxFee)
                    ->setIsActive($currency->IsActive)
                    ->setCoinType($currency->CoinType)
                    ->setBaseAddress($currency->BaseAddress)
                    ->setNotice($currency->Notice);
            }

            $this->manager->flush();

            $this->updateLastUpdationTime(self::SC_CURRENCIES_UPDATION_LAST_TIME,
                new \DateTime('now'));
        }

        return $this;
    }

    public function updateMarketHistories()
    {
        $ago90sec = new \DateTime('90 secs ago');

        $markets = $this->manager->getRepository('AppBundle:Market')
            ->createQueryBuilder('m')
            ->where('m.isTracking = true AND (m.historyUpdatedAt < :ago90secs OR m.historyUpdatedAt IS NULL)')
            ->orderBy('m.historyUpdatedAt')
            ->setParameter('ago90secs', $ago90sec->format('Y-m-d H:i:s'))
            ->setMaxResults(50)
            ->getQuery()
            ->getResult()
            ;

        foreach ($markets as $market) {
            $apiTransactions = $this->publicApi->getMarketHistory($market->getName());

            if (null !== $apiTransactions) {
                $transactionIds = array_map(function($transaction) {
                    return $transaction->Id;
                },$apiTransactions);

                $query = $this
                    ->manager
                    ->createQuery('SELECT mt.transactionId FROM AppBundle:MarketTrade AS mt WHERE mt.transactionId IN (:ids)')
                    ->setParameter('ids', $transactionIds)
                    ;
                $persistedIds = array_map(function($data) {
                    return $data['transactionId'];
                }, $query->getResult());

                foreach ($apiTransactions as $apiTransaction) {
                    if (!in_array($apiTransaction->Id, $persistedIds)) {
                        $transaction = new MarketTrade();
                        $transaction
                            ->setMarket($market)
                            ->setTransactionId($apiTransaction->Id)
                            ->setDate(new Carbon($apiTransaction->TimeStamp, '0000'))
                            ->setQuantity($apiTransaction->Quantity)
                            ->setPrice($apiTransaction->Price)
                            ->setTotal($apiTransaction->Total)
                            ->setFillType($apiTransaction->FillType)
                            ->setOrderType($apiTransaction->OrderType);

                        $this->manager->persist($transaction);
                    }
                }
            }

            $market->setHistoryUpdatedAt(new \DateTime('now'));
        }

        $this->manager->flush();
    }

    /**
     * @return \DateTime|static
     */
    private function getNextUpdationTime($lastTimeName, $lapName, $defaultLap)
    {
        $systemConfigRepository = $this->manager->getRepository('AppBundle:SystemConfig');

        $lastUpdationTime = $systemConfigRepository->getValue($lastTimeName);

        if (null === $lastUpdationTime) {
            return new \DateTime('-1 minute');
        }

        $lastUpdationTime = new \DateTime($lastUpdationTime);

        $lapSecs = $systemConfigRepository->getValue($lapName, $defaultLap);

        $nextUpdationTime = $lastUpdationTime->add(new \DateInterval(sprintf('PT%sS', $lapSecs)));

        return $nextUpdationTime;
    }

    private function updateLastUpdationTime($lastTimeName, \DateTime $lastTime)
    {
        $this->manager->getRepository('AppBundle:SystemConfig')
            ->setValue($lastTimeName, $lastTime->format('Y-m-d H:i:s'));
    }
}
