<?php

namespace AppBundle\Form\Type;

use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * MarketFilterFormType
 *
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class MarketFilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextFilterType::class, array(
                'condition_pattern' => FilterOperands::STRING_CONTAINS
            ))
            ->add('isTracking', ChoiceFilterType::class, array(
                'choices' => array(
                    'Yes' => 'yes',
                    'No' => 'no'
                ),
                'choices_as_values' => true,
                'apply_filter' => function(QueryInterface $filterQuery, $field, $values) {
                    if (empty($values['value'])) {
                        return null;
                    }

                    $expression = $filterQuery->getExpr()->eq($field, $filterQuery->getExpr()->literal($values['value'] === 'yes'));

                    return $filterQuery->createCondition($expression);
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('filtering'),
            'csrf_protection' => false
        ));
    }
}
