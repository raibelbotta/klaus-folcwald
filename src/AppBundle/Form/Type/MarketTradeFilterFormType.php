<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Market;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\DateRangeFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * MarketTradeFilterFormType
 *
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class MarketTradeFilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('market', EntityFilterType::class, array(
                'class' => Market::class
            ))
            ->add('date', DateRangeFilterType::class, array(
                'left_date_options' => array(
                    'format' => 'dd/MM/yyyy',
                    'html5' => false,
                    'widget' => 'single_text'
                ),
                'right_date_options' => array(
                    'format' => 'dd/MM/yyyy',
                    'html5' => false,
                    'widget' => 'single_text'
                )
            ))
            ->add('orderType', ChoiceFilterType::class, array(
                'choices' => array(
                    'Buy' => 'BUY',
                    'Sell' => 'SELL'
                )
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'validation_groups' => 'filtering'
        ));
    }
}
