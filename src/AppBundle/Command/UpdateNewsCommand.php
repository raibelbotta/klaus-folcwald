<?php

namespace AppBundle\Command;

use AppBundle\Entity\CurrencyNews;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * UpdateNewsCommand
 *
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class UpdateNewsCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('app:news:update');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->updateNews();

        $this->saveLastExecutionDate();
    }

    private function saveLastExecutionDate()
    {
        $manager = $this->getContainer()->get('doctrine.orm.entity_manager');

        $manager->getRepository('AppBundle:SystemConfig')->setValue('LAST_NEWS_UPDATE_EXECUTION_DATE', date('Y-m-d H:i:s'));
    }

    private function updateNews()
    {
        $manager = $this->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($this->getCurrencies() as $currency) {
            $currencyNews = new CurrencyNews();

            $currencyNews
                ->setCurrency($currency)
                ->setCountByFullName($this->getResultsCountByNewsText($currency->getLongName()))
                ->setCountByShortname($this->getResultsCountByNewsText($currency->getShortName()));

            $manager->persist($currencyNews);

            $currency->setNewsUpdatedAt(new \DateTime());
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    private function getCurrencies()
    {
        $manager = $this->getContainer()->get('doctrine.orm.entity_manager');

        $currencies = $manager->getRepository('AppBundle:Currency')
            ->createQueryBuilder('c')
            ->where('c.newsUpdatedAt < :tenMinutesAgo')
            ->orderBy('c.newsUpdatedAt')
            ->setParameter('tenMinutesAgo', Carbon::now()->subMinutes(10))
            ->getQuery()
            ->setMaxResults(20)
            ->getResult();

        return $currencies;
    }

    private function getResultsCountByNewsText($text)
    {
        $api = $this->getContainer()->get('app.google_news.api.connection');

        $response = null;
        $tries = 0;

        while (!$response && ($tries < 3)) {
            try {
                $response = $api->getEverything($text);
            } catch (\Exception $e) {
                sleep(1);
                $tries++;
            }

            if ($tries == 3) {
                throw $e;
            }
        }

        return $response->totalResults;
    }
}
