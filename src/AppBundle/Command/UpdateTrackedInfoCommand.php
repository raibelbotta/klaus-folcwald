<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * UpdateTrackedInfoCommand
 *
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class UpdateTrackedInfoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:bittrex:update');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('app.services.bittrex_updater')->updateMarkets();
        $this->getContainer()->get('app.services.bittrex_updater')->updateCurrencies();
        $this->getContainer()->get('app.services.bittrex_updater')->updateMarketHistories();
    }
}
