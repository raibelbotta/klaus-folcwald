<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * LoadUserData
 *
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class LoadUserData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user
            ->setEmail('admin@noteify.us')
            ->setPlainPassword('admin.noteify.us')
            ->setEnabled(true);

        $userManager->updateUser($user);
    }
}
