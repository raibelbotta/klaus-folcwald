<?php

namespace AppBundle\Bittrex\API;

/**
 * Result
 *
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class Result
{
    private $success;

    private $msg;

    private $result;
}
