<?php

namespace AppBundle\Bittrex\API;

/**
 * PublicMethods
 *
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class PublicMethods extends Connection
{
    public function getMarkets()
    {
        return $this->sendRequest('/public/getmarkets');
    }

    public function getCurrencies()
    {
        return $this->sendRequest('/public/getcurrencies');
    }

    public function getMarketHistory($marketName)
    {
        return $this->sendRequest('/public/getmarkethistory', array('market' => $marketName));
    }
}
