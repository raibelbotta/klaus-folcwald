<?php

namespace AppBundle\Bittrex\API;

/**
 * Connection
 *
 * @author Raibel Botta <raibelbotta@gmail.com>
 */
class Connection
{
    const END_POINT = 'https://bittrex.com/api';
    const VERSION = '/v1.1';

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $apiSecret;

    public function __construct($apiKey, $apiSecret)
    {
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
    }

    protected function sendSignedRequest($methodName)
    {
        $nonce = time();

        $uri = sprintf('%s%s%s?apikey=%s&nonce=%s',self::END_POINT, self::VERSION, $methodName, $this->apiKey, $nonce);
        $sign = hash_hmac('sha512', $uri, $this->apiSecret);

        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(sprintf('apisign:%s', $sign)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $execResult = curl_exec($ch);

        $obj = json_decode($execResult);

        return $obj->result;
    }

    protected function sendRequest($methodName, array $params = array())
    {
        $paramPairs = array();
        foreach ($params as $name => $value) {
            $paramPairs[] = sprintf('%s=%s', $name, $value);
        }

        $uri = sprintf('%s%s%s%s',self::END_POINT, self::VERSION, $methodName, count($paramPairs) > 0 ? '?' . implode('&', $paramPairs) : '');

        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $execResult = curl_exec($ch);

        $obj = json_decode($execResult);

        return $obj->result;
    }
}
